package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class PlayState extends FlxState
{

	var level:Level;
	var player:Player;
	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	override public function create():Void
	{
		FlxG.mouse.visible = false;

		level = new Level("assets/maps/Test.tmx");

		player = new Player();

		add(level.backgroundGroup);
		add(level.collisionGroup);
		add(player);
		add(level.foregroundGroup);

		level.mobGroup.add(player);	

		FlxG.camera.bounds = level.getBounds();
		FlxG.worldBounds.copyFrom(level.getBounds());
		FlxG.camera.follow(player);

		super.create();
	}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		Reg.keys.update();
		level.update();
		super.update();
	}	
}
