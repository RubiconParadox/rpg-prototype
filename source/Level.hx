package;

import haxe.io.Path;
import haxe.xml.Parser;
import flixel.addons.editors.tiled.TiledLayer;
import flixel.addons.editors.tiled.TiledMap;
import flixel.addons.editors.tiled.TiledObject;
import flixel.addons.editors.tiled.TiledObjectGroup;
import flixel.addons.editors.tiled.TiledTile;
import flixel.addons.editors.tiled.TiledTileSet;
import flixel.addons.tile.FlxTileSpecial;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.group.FlxTypedGroup;
import flixel.tile.FlxTilemap;
import flixel.util.FlxRandom;
import flixel.util.FlxRect;
import flixel.util.FlxSort;

class Level extends TiledMap
{
	private inline static var PATH_TILESETS = "assets/maps/";
	
	public var backgroundGroup:FlxTypedGroup<FlxTilemap>;
	public var foregroundGroup:FlxTypedGroup<FlxTilemap>;
	public var collisionGroup:FlxTypedGroup<FlxTilemap>;

	public var mobGroup:FlxTypedGroup<Mob>;

	private var bounds:FlxRect;

	public function new(level:Dynamic)
	{
		super(level);

		backgroundGroup = new FlxTypedGroup<FlxTilemap>();
		foregroundGroup = new FlxTypedGroup<FlxTilemap>();

		collisionGroup = new FlxTypedGroup<FlxTilemap>();
		mobGroup = new FlxTypedGroup<Mob>();

		bounds = FlxRect.get(0, 0, fullWidth, fullHeight);

		for (tileLayer in layers)
		{
			var tileSheetName:String = tileLayer.properties.get("tileset");

			if (tileSheetName == null)
				throw "'tileset' property not defined for the '" + tileLayer.name + "' layer. Please add the property to the layer.";
			
			var tileSet:TiledTileSet = null;
			for (ts in tilesets)
			{
				if (ts.name == tileSheetName)
				{
					tileSet = ts;
					break;
				}
			}

			if (tileSet == null) 
				throw "Tileset '" + tileSheetName + "' not found. Did you mispell the 'tilesheet; property in " + tileLayer.name + "' layer?";

			var imagePath = new Path(tileSet.imageSource);
			var processedPath = PATH_TILESETS + imagePath.file + "." + imagePath.ext;

			var tilemap:FlxTilemap = new FlxTilemap();
			tilemap.widthInTiles = width;
			tilemap.heightInTiles = height;	
			tilemap.loadMap(tileLayer.tileArray, processedPath, tileSet.tileWidth, tileSet.tileHeight, 0, 1, 1, 1);
			
			if (tileLayer.properties.contains("bg"))
			{
				backgroundGroup.add(tilemap);
			}
			if (tileLayer.properties.contains("fg"))
			{
				foregroundGroup.add(tilemap);
			}
			if (tileLayer.properties.contains("col"))
			{
				collisionGroup.add(tilemap);
			}
		}
	}

	public function update():Void
	{
		updateCollisions();
		updateEventsOrder();
	}

	public function updateEventsOrder():Void
   	{
		//mobGroup.sort(FlxSort.byY);
	}

	public function updateCollisions():Void
	{
		FlxG.collide(collisionGroup, mobGroup); 
		FlxG.collide(mobGroup, mobGroup);
	}		

	public function getBounds():FlxRect
	{
		return bounds;
	}

	private inline function isSpecialTile(tile:TiledTile, animations:Dynamic):Bool
	{
		return (tile.isFlipHorizontally || tile.isFlipVertically || tile.rotate != FlxTileSpecial.ROTATE_0 || animations.exists(tile.tilesetID));
	}
}
