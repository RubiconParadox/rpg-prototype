package;

import flixel.FlxG;

class Input
{
	public var up:Bool = false;
	public var down:Bool = false;
	public var left:Bool = false;
	public var right:Bool = false;
	public var actionOne:Bool = false;
	public var actionTwo:Bool = false;

	public function update():Void
	{
		getKeyStates();
	}

	public function getKeyStates():Void
	{
		up = false;
		down = false;
		left = false;
		right = false;
		actionOne = false;
		actionTwo = false;

		up = FlxG.keys.anyPressed(["UP","W"]);
		down = FlxG.keys.anyPressed(["DOWN","S"]);
		left = FlxG.keys.anyPressed(["LEFT","A"]);
		right = FlxG.keys.anyPressed(["RIGHT","D"]);
		actionOne = FlxG.keys.anyPressed(["Z"]);
		actionTwo = FlxG.keys.anyPressed(["X"]);
	}

}
