package;

import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxG;

class Player extends Mob
{
	public function new(X:Float = 0, Y:Float = 0)
	{
		super(X,Y);
		_speed = 5;
		maxVelocity.x = 160;
		maxVelocity.y = 160;
		drag.x = maxVelocity.x * 4;
		drag.y = maxVelocity.y *4;

		loadGraphic("assets/images/Player.png");
		offset.y = 16;
		height = 16;

	}

	override public function update():Void
	{
		manageInput();
		super.update();
	}

	override public function destroy():Void
	{
		super.destroy();
	}

	private function manageInput():Void
	{
		acceleration.set(0, 0);
		if(Reg.keys.up)
		{
			acceleration.y = -drag.y;
		}
		if(Reg.keys.down)
		{
			acceleration.y = drag.y;
		}
		if(Reg.keys.left)
		{
			acceleration.x = -drag.x;
		}
		if(Reg.keys.right)
		{
			acceleration.x = drag.x;
		}
	}
}
