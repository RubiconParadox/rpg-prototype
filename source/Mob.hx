package;

import flixel.FlxSprite;
import flixel.FlxObject;
import flixel.FlxG;

class Mob extends FlxSprite
{
	private var _speed:Float = 1;

	public function new(X:Float = 0, Y:Float = 0, ?Speed:Float = 1)
	{
		super(X, Y);
		_speed = Speed;
	}

	override public function update():Void
	{
		super.update();
	}

	override public function destroy():Void
	{
		super.destroy();
	}

	private function checkMapBounds():Void
	{
		
	}
}
